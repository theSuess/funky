default: build

build: clean
	mcs -out:main.exe src/Main.cs src/Parser.cs src/Prelude.cs

run: build
	mono main.exe

test: build
	mono main.exe example.fn
	mono main.exe hof.fn

clean:
	-rm -f Main.exe
