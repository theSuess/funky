using System;
using System.Text.RegularExpressions;
using System.Runtime.Remoting;
using System.Collections.Generic;

namespace Funky
{
   class Program
   {
       public static void Main(string[] args)
       {
           Parser p = new Parser();
           if(args.Length == 0)
           {
               Console.WriteLine("Welcome to the Funky REPL!");
               string line = Console.ReadLine();
               while(line != null && line != ":q")
               {
                   if(Regex.IsMatch(line.Trim(),"^:l .*"))
                   {
                       RunScript(p,line.Split(new []{":l "},StringSplitOptions.None)[1].Trim()); // [1] because the split returns the empty string before ":l"
                   }
                   else
                   {
                       PrintWithType(p.Execute(line));
                   }
                   line = Console.ReadLine();
               }
           }
           else
           {
               RunScript(p,args[0]);
           }
       }
       public static void RunScript(Parser p, string s)
       {
           string[] code = System.IO.File.ReadAllLines(s);
           foreach(var line in code)
           {
               p.Execute(line);
           }
       }
       public static void PrintWithType(dynamic v)
       {
           if(v.GetType().ToString() == "System.String")
           {
               Console.Write('"');
               Console.Write(v);
               Console.Write('"');
           }
           else
           {
               Console.Write(v);
           }
           Console.Write("\t:: ");
           Console.WriteLine(v.GetType());
       }
       public static void PrintWithType(dynamic[] vs)
       {
           foreach(var el in vs)
           {
               PrintWithType(el);
           }
       }
   }
}
