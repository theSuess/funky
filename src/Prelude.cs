using System;
using System.Runtime.Remoting;
using System.Collections.Generic;

namespace Funky
{
    class Prelude
    {
        public static dynamic Id(dynamic arg)
        {
            return arg;
        }
        public static Func<dynamic,dynamic> Plus(dynamic arg)
        {
            return (argument) => {return arg + argument;};
        }
        public static Func<dynamic,dynamic> Minus(dynamic arg)
        {
            return (argument) => {return argument - arg;};
        }
        public static Func<dynamic,dynamic> Times(dynamic arg)
        {
            return (argument) => {return argument * arg;};
        }
        public static Func<dynamic,dynamic> Map(Func<dynamic,dynamic> f)
        {
            return delegate(dynamic list)
            {
                var nlist = new List<dynamic>();
                foreach(var el in list)
                {
                    nlist.Add(f(el));
                }
                return nlist;
            };
        }
        public static bool Put(dynamic v)
        {
            if(v.GetType() == typeof(List<dynamic>))
            {
                foreach(var elem in v)
                {
                    Console.Write(elem);
                    Console.Write(" ");
                }
                Console.WriteLine();
                return true;
            }
            Console.WriteLine(v);
            return true;
        }
        public static Func<Func<dynamic,dynamic>,dynamic> Compose(Func<dynamic,dynamic> f1)
        {
            return delegate(Func<dynamic,dynamic> f2)
            {
                return new Func<dynamic,dynamic>(ComposeInternal(f1,f2));
            };
        }
        private static Func<dynamic,dynamic> ComposeInternal(Func<dynamic,dynamic> f1,Func<dynamic,dynamic> f2)
        {
            return delegate(dynamic arg)
            {
                return f1(f2(arg));
            };
        }
    }
}
