using System;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;

namespace Funky
{
    class Parser
    {
        private Dictionary<string,dynamic> environment = new Dictionary<string,dynamic>();
        private Dictionary<string,Func<dynamic,dynamic>> funcs = new Dictionary<string,Func<dynamic,dynamic>>();
        public Parser(){}
        public Parser(Dictionary<string,dynamic> env)
        {
            environment = env;
        }
        public Func<dynamic,dynamic> BuildCompose(List<dynamic> args)
        {
            return delegate(dynamic arg)
            {
                dynamic tvar = arg;
                foreach(var f in args)
                {
                    tvar = f(tvar);
                }
                return tvar;
            };
        }
        public string[] RemoveEmpty(string[] s)
        {
            return s.Where(x => !string.IsNullOrEmpty(x) && x != " ").ToArray();
        }
        public Func<dynamic,dynamic> BuildFunction(string s)
        {
            var appl = s.Trim().Split(' '); // Split in function name and application value
            if(Regex.IsMatch(appl[0],"^\\^.*"))
            {
                var fn = ToHOFunction(appl[0]);
                return fn(ConvertToType(appl[1]));
            }
            else if(Regex.IsMatch(s.Trim(),".*::Fun"))
            {
                return ConvertToType(s);
            }
            else
            {
                var fn = ToFunction(appl[0]);
                return fn(ConvertToType(appl[1]));
            }
        }
        public Func<dynamic,dynamic> BuildApplication(string s)
        {
            var term = RemoveEmpty(s.Split('(',')'));
            List<dynamic> fns = new List<dynamic>();
            foreach(var fn in term)
            {
                fns.Add(BuildFunction(fn));
            }
            return BuildCompose(fns);
        }
        public dynamic Execute(string s)
        {
            s = s.Trim();
            //let x   = 4::Int
            //[0] [1] = [1](different split)
            if (Regex.IsMatch(s,"^let .* = .*"))
            {
                var value = s.Split('=')[1].Trim();
                var name = s.Split(' ')[1].Trim();
                StoreVar(name,Execute(value));
                return true;
            }
            else if(Regex.IsMatch(s,"^def .* = .*"))
            {
                var value = s.Split('=')[1].Trim();
                var name = s.Split(' ')[1].Trim();
                DefFunc(name,BuildFunction(value));
                return true;
            }
            else if(Regex.IsMatch(s,"^put .*"))
            {
                var value = s.Substring(4);
                Prelude.Put(Execute(value));
                return true;
            }
            else if(Regex.IsMatch(s,"^[a-zA-Z0-9,\\[\\]]*::.*"))
            {
                return ConvertToType(s);
            }
            var ps = RemoveEmpty(s.Split('{','}'));
            return BuildApplication(ps[0])(ConvertToType(ps[1]));
        }
        public Func<dynamic,dynamic> ToFunction(string s)
        {
            switch(s)
            {
                case "+":
                    return Prelude.Plus;
                case "-":
                    return Prelude.Minus;
                case "*":
                    return Prelude.Times;
                default:
                    throw new NotImplementedException(s);
            }
        }
        public Func<Func<dynamic,dynamic>,dynamic> ToHOFunction(string s)
        {
            switch(s)
            {
                case "^§":
                    return Prelude.Map;
                default:
                    throw new NotImplementedException(s);
            }
        }
        private dynamic ConvertToType(string s)
        {
            var dec = Regex.Split(s,"::");
            switch(dec[1])
            {
                case "Int":
                    return Convert.ToInt32(dec[0]);
                case "Double":
                    return Convert.ToDouble(dec[0]);
                case "Var":
                    return environment[dec[0].Trim()];
                case "[Int]":
                    var l = new List<int>();
                    foreach(var elem in dec[0].Trim().Substring(1,dec[0].Length - 3).Split(',')) // -3 because c# is wonky
                    {
                        l.Add(Convert.ToInt32(elem));
                    }
                    return l;
                case "Fun":
                    return funcs[dec[0].Trim()];
                default:
                    throw new NotImplementedException();
            }
        }
        private void StoreVar(string name, dynamic v)
        {
            environment.Add(name,v);
        }
        private void DefFunc(string name,Func<dynamic,dynamic> fun)
        {
            funcs.Add(name,fun);
        }
    }
}
